#  ![python_technologie](images/Python_technologie.png) **Les tests**

## ![python_titre_bleu](images/Python_titre_bleu.png) **Les tests en vidéos**

[![if](images/if.png)](https://youtu.be/hPhQUUgy6qI) [![if](images/if2.png)](https://youtu.be/0JXc48GXZrU) 

## ![python_titre_bleu](images/Python_titre_bleu.png) **Définition**

Les tests sont un élément essentiel à tout langage informatique si on veut lui donner un peu de complexité car ils permettent
à l’ordinateur de prendre des décisions. Pour cela, Python utilise l’instruction if ainsi qu’une comparaison.


```python
>>> nombre = 5
```


```python
>>> if nombre == 5 :
...     print("Test vrai")
...
```

    Test vrai
    


```python
>>> if nombre == 4 :
...     print("Test faux")
...
```

> Dans le premier exemple, le test étant vrai, l’instruction ``print("Test vrai !")`` est exécutée. Dans le
second exemple, le test est faux et rien n’est affiché.
>
> Les blocs d’instructions dans les tests doivent forcément être indentés comme pour les boucles ``for`` et ``while``. L’indentation indique la portée des instructions à exécuter si le test est vrai.
>
> Comme avec les boucles ``for`` et ``while``, la ligne qui contient l’instruction ``if`` se termine par le caractère deux-points.


```python
if nombre != 5 :
    print("Nombre est différent de 5")
if nombre == 5 :
    print("Nombre vaut 5")
```

    Nombre vaut 5
    

![apprendre](images/apprendre.png) **Observe attentivement et explique**


```python
nombre = 1
if nombre :
    print("Nombre est différent de 0")
```

    Nombre est différent de 5
    


```python
nombre = 0
if nombre :
    print("Nombre est différent de 0")
else :
    print("Nombre vaut 0")
```

    Nombre vaut 0
    

## ![python_titre_bleu](images/Python_titre_bleu.png) **if else**

il est pratique de tester si la condition est vraie ou si elle est fausse dans une même instruction ``if``. Plutôt que
d’utiliser deux instructions if, on peut se servir des instructions ``if`` et ``else`` :


```python
if nombre != 5 :
    print("Nombre est différent de 5")
else :
    print("Nombre vaut 5")
```

    Nombre vaut 5
    

On peut utiliser une série de tests dans la même instruction if, notamment pour tester plusieurs valeurs d’une même
variable.


```python
import random

personnageStarWars = random.choice(["Dooku", "Anakin Skywalker", "Sheev Palpatine" , "Rey"])
if personnageStarWars == "Dooku":
    print ("Commandant suprême de la Confédération des systèmes indépendants.")
elif personnageStarWars == "Sheev Palpatine":
    print ("Seigneur noir des Sith ")
elif personnageStarWars == "Rey":
    print ("Jeune femme de la planète Jakku, survivant en vendant des pièces détachées des vieux vaisseaux de l'Empire")
else : 
    print ("Il deviendra Darth Vader")
```

    Seigneur noir des Sith 
    

## ![python_titre_bleu](images/Python_titre_bleu.png) **OU | ET ~ or | and**

Les tests multiples permettent de tester plusieurs conditions en même temps en utilisant des opérateurs booléens. Les deux
opérateurs les plus couramment utilisés sont le **ET** et le **OU**. 

| Condition | Opérateur | Condition | Résultat |
|:--:|:--:|:--:|:--:|
|Vrai| ET |Vrai| Vrai|
|Vrai|ET|Faux|Faux|
|Faux|ET|Vrai|Faux|
|Faux|ET|Faux|Faux|


```python
nombre = 3
mot = "Star Wars"

if nombre == 3 and mot == "Star Wars" :
    print("Vrai")
```

    Vrai
    

| Condition | Opérateur | Condition | Résultat |
|:--:|:--:|:--:|:--:|
|Vrai| OU |Vrai| Vrai|
|Vrai|OU|Faux|Vrai|
|Faux|OU|Vrai|Vrai|
|Faux|OU|Faux|Faux|


```python
nombre = 3
mot = "Star Wars"

if nombre == 3 or mot == "Jedi" :
    print("Vrai")
```

    Vrai
    


```python
nombre = 3
mot = "Star Wars"

if not nombre or mot == "Star Wars" :
    print("Vrai")
```

    Vrai
    

![apprendre](images/apprendre.png) **Observe attentivement et explique**


```python
nombre = 3
mot = "Star Wars"

if nombre == 3 or mot2 == "Star Wars" :
    print("Vrai")
```

    Vrai
    

> La variable mot2 n'est pas définie et pourtant Python ne renvoie pas d'erreur.
>
> nombre == 3 or mot2 == "Star Wars" est vraie car nombre == 3 est vrai. mot2 == "Star Wars" n'est dans ce cas pas évalué par Python.


```python
nombre = 3
mot = "Star Wars"

if nombre2 == 3 or mot == "Star Wars" :
    print("Vrai")
```


    ---------------------------------------------------------------------------

    NameError                                 Traceback (most recent call last)

    <ipython-input-35-dbcee3db0897> in <module>
          2 mot = "Star Wars"
          3 
    ----> 4 if nombre2 == 3 or mot == "Star Wars" :
          5     print("Vrai")
    

    NameError: name 'nombre2' is not defined


## ![python_titre_bleu](images/Python_titre_bleu.png) **break | continue**

Ces deux instructions permettent de modifier le comportement d’une boucle ``for`` ou ``while`` avec un test.

**L’instruction ``break`` stoppe la boucle.**


```python
for i in range(10):
    if i % 2 :
        break
    print(i)
```

    0
    

**L’instruction ``continue`` saute à l’itération suivante, sans exécuter la suite du bloc d’instructions de la boucle.**


```python
for i in range(10):
    if i % 2 :
        continue
    print(i)
```

    0
    2
    4
    6
    8
    

## ![python_titre_bleu](images/Python_titre_bleu.png) **Exercice 1**

**L’implication logique p ⇒ q est le booléen : NON(p) OU q**


```python
def implication(p, q):
    return(not p or q)

boolean = [True, False]
p, q, imp = 'p', 'q', '\u21D2'

print(f'|{p:^5}|{q:^5}|{imp:^5}|')

for i in boolean:
    for j in boolean:
        print(f'|{i:^5}|{j:^5}|{implication(i, j):^5}|')
```

    |  p  |  q  |  ⇒  |
    |  1  |  1  |  1  |
    |  1  |  0  |  0  |
    |  0  |  1  |  1  |
    |  0  |  0  |  1  |
    

a. **L’équivalence logique p ⇔ q est le booléen : (p ⇒ q) ET (q ⇒ p)**

Écrire une fonction ``equivalence(p, q)`` en Python qui retourne (p ⇒ q) ET (q ⇒ p), p et q étant deux booléens.
On utilisera la fonction ``implication(p, q)``.

b. **Le OU exclusif (noté XOR ou ⊕) p XOR q est le booléen : (p OU q) ET NON (p ET q)**

Écrire une fonction ``xor(p, q)`` en Python qui retourne (p OU q) ET NON (p ET q), p et q étant deux booléens.

## ![python_titre_bleu](images/Python_titre_bleu.png) **Exercice 2**

Constituez une liste semaine contenant le nom des sept jours de la semaine.

En utilisant une boucle, écrivez chaque jour de la semaine ainsi que les messages suivants :

+ Au travail s’il s’agit du lundi au jeudi;
+ Chouette c'est vendredi s’il s’agit du vendredi;
+ Repos ce week-end s’il s’agit du samedi ou du dimanche.

Ces messages ne sont que des suggestions, vous pouvez laisser libre cours à votre imagination.

## ![python_titre_bleu](images/Python_titre_bleu.png) **Exercice 3**

La fonction ``min()`` de Python renvoie l’élément le plus petit d’une liste constituée de valeurs numériques ou de chaînes de
caractères. Sans utiliser cette fonction, créez un script qui détermine le plus petit élément de la liste ``[10, 9, 17, 3, 1]``.

# ![projet](images/yoda.png)  **Recherche du number's Yoda par dichotomie**

La recherche par dichotomie est une méthode qui consiste à diviser (en général en parties égales) un problème pour en trouver la solution. À titre d’exemple, voici une discussion entre Vador et Yoda dans laquelle Vador essaie de deviner le nombre (compris entre 1 et 100 inclus) auquel Yoda a pensé.

+ **[Yoda]** « C’est bon, j’ai pensé à un nombre entre 1 et 100. »
+ **[Vador]** « OK, je vais essayer de le deviner. Est-ce que ton nombre est plus petit ou plus grand que 50 ? »
+ **[Yoda]** « Plus grand. »
+ **[Vador]** « Est-ce que ton nombre est plus petit, plus grand ou égal à 75 ? »
+ **[Yoda]** « Plus grand. »
+ **[Vador]** « Est-ce que ton nombre est plus petit, plus grand ou égal à 87 ? »
+ **[Yoda]** « Plus petit. »
+ **[Vador]** « Est-ce que ton nombre est plus petit, plus grand ou égal à 81 ? »
+ **[Yoda]** « Plus petit. »
+ **[Vador]** « Est-ce que ton nombre est plus petit, plus grand ou égal à 78 ? »
+ **[Yoda]** « Plus grand. »
+ **[Vador]** « Est-ce que ton nombre est plus petit, plus grand ou égal à 79 ? »
+ **[Yoda]** « Égal. C’est le nombre auquel j’avais pensé. Bravo ! »

Pour arriver rapidement à deviner le nombre, l’astuce consiste à prendre à chaque fois la moitié de l’intervalle dans lequel
se trouve le nombre. Voici le détail des différentes étapes :

1. le nombre se trouve entre 1 et 100, on propose 50 (100 / 2).
2. le nombre se trouve entre 50 et 100, on propose 75 ( 50 + (100-50)/2 ).
3. le nombre se trouve entre 75 et 100, on propose 87 ( 75 + (100-75)/2 ).
4. le nombre se trouve entre 75 et 87, on propose 81 ( 75 + (87-75)/2 ).
5. le nombre se trouve entre 75 et 81, on propose 78 ( 75 + (81-75)/2 ).
6. le nombre se trouve entre 78 et 81, on propose 79 ( 78 + (81-78)/2 ).

Créez un script qui reproduit ce jeu de devinettes. Vous pensez à un nombre entre 1 et 100 et l’ordinateur essaie de le
deviner par dichotomie en vous posant des questions.
Votre programme utilisera la fonction ``input()`` pour interagir avec l’utilisateur.

## **Une solution possible**

```python
from math import floor

def numberYoda() :
    print("C’est bon, j’ai pensé à un nombre entre 1 et 100.")
    min = 1
    max = 100
    vadorProposition = floor((min + max)/2)
    indice = input(f"OK, je vais essayer de le deviner. Est-ce que ton nombre est plus petit ou plus grand que {vadorProposition} ? ")
    while indice != "=" :
        if indice == "+" :
            min = vadorProposition
        else :
            max = vadorProposition
        vadorProposition = floor((min + max)/2)
        indice = input(f"Est-ce que ton nombre est plus petit ou plus grand que {vadorProposition} ? ")
    print(f"Égal. C’est le nombre auquel j’avais pensé, {vadorProposition}. Bravo !")
```


