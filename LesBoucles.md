# ![python_technologie](images/Python_technologie.png) **Les boucles**


## ![python_titre_bleu](images/Python_titre_bleu.png) **La boucle for en vidéo**

[![boucles](images/boucles.png)](https://youtu.be/oZM5w2VxaYk)

## ![python_titre_bleu](images/Python_titre_bleu.png) **La boucle for**

En programmation, on est souvent amené à répéter plusieurs fois une instruction. Incontournables à tout langage de programmation,
les boucles vont nous aider à réaliser cette tâche de manière compacte et efficace.
Imaginez par exemple que vous souhaitiez afficher les caractères de la chaîne de caractères ``DarkSATHI``. Dans l’état actuel de
vos connaissances, il faudrait taper quelque chose du style :


```python
nom = 'DarkSATHI'
print(nom[0])
print(nom[1])
print(nom[2])
print(nom[3])
print(nom[4])
print(nom[5])
print(nom[6])
print(nom[7])
print(nom[8])
```

    D
    a
    r
    k
    S
    A
    T
    H
    I
    

Si votre chaîne ne contient que 4 caractères, ceci est encore faisable mais imaginez qu’elle en contienne 100 voire 1000! Pour
remédier à cela, il faut utiliser les boucles. Regardez l’exemple suivant :


```python
for lettre in nom :
    print(lettre)
```

    D
    a
    r
    k
    S
    A
    T
    H
    I
    

Commentons en détails ce qu’il s’est passé dans cet exemple :
La variable ``lettre`` est appelée variable d’itération, elle prend successivement les différentes valeurs des caractères de la chaîne ``nom``
à chaque itération de la boucle. Celle-ci est créée par Python la première fois que la ligne contenant le for est exécutée (si elle existait déjà son
contenu serait écrasé). Une fois la boucle terminée, cette variable d’itération ``lettre`` ne sera pas détruite et contiendra ainsi
le dernier caractère de la chaîne (ici le caractère ``I``).
Notez bien les types des variables utilisées ici : ``nom`` est une chaîne de caractères sur laquelle on itère, et lettre est une chaîne
de caractères car chaque caractère de ``nom`` est aussi aussi de type ``<class str>``. En Python, une boucle itère toujours sur un objet dit séquentiel
(c’est-à-dire un objet constitué d’autres objets). 
D’ores et déjà, prêtez attention au caractère deux-points « : » à la fin de la ligne débutant par for. Cela signifie que la
boucle for attend un **bloc d’instructions**, en l’occurrence toutes les instructions que Python répétera à chaque itération de la
boucle. On appelle ce bloc d’instructions le corps de la boucle. 

Comment indique-t-on à Python où ce bloc commence et se
termine ? Cela est signalé uniquement par **l’indentation**, c’est-à-dire le décalage vers la droite de la (ou des) ligne(s) du bloc
d’instructions.


```python
print(lettre)
```

    I
    

### ![python_dossier](images/python_dossier.png) **Oublie de l'indentation**

Si on oublie l’indentation, Python renvoie un message d’erreur :


```python
for lettre in nom :
print(lettre)
```


      File "<ipython-input-4-23e2b9fcda27>", line 2
        print(lettre)
            ^
    IndentationError: expected an indented block
    


### ![python_dossier](images/python_dossier.png) **La fonction range()**

La fonction range() est une fonction spéciale en Python qui génère des nombres entiers compris dans un intervalle.


```python
for i in range(10):
    print(i)
```

    0
    1
    2
    3
    4
    5
    6
    7
    8
    9
    

## ![python_titre_bleu](images/Python_titre_bleu.png) **Les comparaisons**

Python est capable d’effectuer toute une série de comparaisons entre le contenu de deux variables, telles que :

|Syntaxe Python|Signification
|:--:|:--:|
|**==**|égal à|
|**!=**|différent de|
|**>**|supérieur à|
|**>=**|supérieur ou égal à|
|**<**|inférieur à|
|**<=**|inférieur ou égal à|

Observez les exemples suivants avec des nombres entiers.


```python
>>> x  = 10
```


```python
>>> x == 10
```




    True




```python
>>> x < 5
```




    False




```python
>>> x > 5
```




    True



> Python renvoie la valeur True si la comparaison est vraie et False si elle est fausse. 
>
> True et False sont des booléens (un nouveau type de variable).
>
> Faites bien attention à ne pas confondre l’opérateur d’affectation = qui affecte une valeur à une variable et l’opérateur de comparaison == qui compare les valeurs de deux variables.

Vous pouvez également effectuer des comparaisons sur des chaînes de caractères.


```python
>>> nom == 'DARKSATHI'
```




    False




```python
>>> nom != 'DARKSATHI'
```




    True



> Dans le cas des chaînes de caractères, a priori seuls les tests == et != ont un sens. En fait, on peut aussi utiliser les
opérateurs <, >, <= et >=. Dans ce cas, l’ordre alphabétique est pris en compte, par exemple :


```python
>>> 'a' < 'e'
```




    True




```python
>>> 'Dark' > 'DARK'
```




    True



Dans ce dernier cas, Python compare les deux chaînes de caractères, caractère par caractère, de la gauche vers la droite (le premier
caractère avec le premier, le deuxième avec le deuxième, etc). Dès qu’un caractère est différent entre l’une et l’autre des deux
chaînes, il considère que la chaîne la plus petite est celle qui présente le caractère ayant le plus petit code ASCII (les caractères
suivants de la chaîne de caractères sont ignorés dans la comparaison).

![image](https://www.sciencebuddies.org/references/ascii-table.png)

## ![python_titre_bleu](images/Python_titre_bleu.png) **La boucle while en vidéo**

[![boucles](images/while.png)](https://youtu.be/JNFFcQcgNj0)

## ![python_titre_bleu](images/Python_titre_bleu.png) **La boucle while**

Une autre alternative à l’instruction for couramment utilisée en informatique est la boucle while. Le principe est simple.
Une série d’instructions est exécutée tant qu’une condition est vraie. Par exemple :


```python
i = 1
while i <= 5:
    print(i)
    i = i + 1
```

    1
    2
    3
    4
    5
    

Remarquez qu’il est encore une fois nécessaire d’indenter le bloc d’instructions correspondant au corps de la boucle (ici,
les instructions lignes 3 et 4).
Une boucle while nécessite généralement trois éléments pour fonctionner correctement :
1. Initialisation de la variable d’itération avant la boucle (ligne 1).
2. Test de la variable d’itération associée à l’instruction while (ligne 2).
3. Mise à jour de la variable d’itération dans le corps de la boucle (ligne 4).

Faites bien attention aux tests et à l’incrémentation que vous utilisez car une erreur mène souvent à des « boucles infinies» qui ne s’arrêtent jamais. Vous pouvez néanmoins toujours stopper l’exécution d’un script Python à l’aide de la combinaison de touches Ctrl-C (c’est-à-dire en pressant simultanément les touches Ctrl et C). Par exemple :


```python
i = 0
while i < 10:
print (" Je suis ton maître !")
```

Ici, nous avons omis de mettre à jour la variable i dans le corps de la boucle. Par conséquent, la boucle ne s’arrêtera jamais
(sauf en pressant Ctrl-C) puisque la condition i < 10 sera toujours vraie.


La boucle while combinée à la fonction input() peut s’avérer commode lorsqu’on souhaite demander à l’utilisateur une
valeur numérique. Par exemple :


```python
i = 0
while i < 10:
    reponse = input (" Entrez un entier supérieur à 10 : ")
    i = int(reponse)
```

-----
##   ![binairelogo](images/binaire_logo.png) Exercices

1. Soit la chaîne de caractères `Quand 900 ans comme moi tu auras, moins en forme tu seras !`. Affichez l’ensemble des caractères de cette chaîne (un caractère par ligne) de trois manières différentes (deux avec for et une avec while).
2. Avec une boucle, affichez les nombres de 1 à 10 sur une seule ligne.
3. Créez un script qui dessine un triangle comme celui-ci :

4. Triangle


```python
def triangle1(tailleBase):
    for i in range(1, tailleBase + 1):
        print('*' * i)

triangle1(10)
```

    *
    **
    ***
    ****
    *****
    ******
    *******
    ********
    *********
    **********
    

5. Triangle inversé : Créez un script qui dessine un triangle comme celui-ci :


```python
def triangle2(tailleBase):
    for i in reversed(range(1, tailleBase + 1)):
        print('*' * i)

triangle2(10)
```

    **********
    *********
    ********
    *******
    ******
    *****
    ****
    ***
    **
    *
    

6. Triangle gauche : Créez un script qui dessine un triangle comme celui-ci :


```python
def triangle3(tailleBase):
    for i in range(1, tailleBase + 1):
        print(' ' * (10 - i) + '*' * i)

triangle3(10)
```

             *
            **
           ***
          ****
         *****
        ******
       *******
      ********
     *********
    **********
    

6. Pyramide : Créez un script qui dessine un triangle comme celui-ci :


```python
def triangle4(tailleBase):
    for i in range(1, tailleBase + 1, 2):
        print(' ' * ((21 - i)//2) + '*' * i + ' ' * ((21 - i)//2))

triangle4(21)
```

              *          
             ***         
            *****        
           *******       
          *********      
         ***********     
        *************    
       ***************   
      *****************  
     ******************* 
    *********************
    
