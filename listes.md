#  ![python_technologie](images/Python_technologie.png) **Les listes en python**

[![image](images/listeVideo.png)](https://youtu.be/aHycOtc8G9k)


## ![python_titre_bleu](images/Python_titre_bleu.png) **Définition et utilisation**

Une liste est une structure de données qui contient une série de valeurs. Python autorise la construction de liste contenant
des valeurs de types différents (par exemple entier et chaîne de caractères), ce qui leur confère une grande flexibilité. Une liste
est déclarée par une série de valeurs (n’oubliez pas les guillemets, simples ou doubles, s’il s’agit de chaînes de caractères)
séparées par des virgules, et le tout encadré par des crochets.


```python
jedi = ['Plo Koon', 'Qui-Gon Jinn', 'Obi-Wan Kenobi', 'Ki-Adi-Mundi', 'Anakin Skywalker', 'Luke Skywalker', 'Ahsoka Tano', 'Mace Windu', 'Yoda']
tailleJedi = [1.88, 1.93, 1.82, 1.98, 1.88, 1.72, 1.88, 1.92, 0.66]
print(jedi)
print(tailleJedi)
```

On appelle un élément d'une liste par sa position. Ce numéro est appelé ```indice```(ou index).

|Éléments|'Plo Koon'|'Qui-Gon Jinn'|'Obi-Wan Kenobi'|'Ki-Adi-Mundi'|'Anakin Skywalker'|'Luke Skywalker'|'Ahsoka Tano'|'Mace Windu'|'Yoda'|
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
|**Indice**|0|1|2|3|4|5|6|7|8|

Il faut être attentif au fait que les indices d’une liste de $n$ éléments commence à 0 et se termine à $n-1$.

La liste peut également être indexée avec des nombres négatifs selon le modèle suivant :

|Éléments|'Plo Koon'|'Qui-Gon Jinn'|'Obi-Wan Kenobi'|'Ki-Adi-Mundi'|'Anakin Skywalker'|'Luke Skywalker'|'Ahsoka Tano'|'Mace Windu'|'Yoda'|
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
|**Indice**|-9|-8|-7|-6|-5|-4|-3|-2|-1|

Les indices négatifs reviennent à compter à partir de la fin. Leur principal avantage est que vous pouvez accéder au dernier
élément d’une liste à l’aide de l’indice -1 sans pour autant connaître la longueur de cette liste. L’avant-dernier élément a lui
l’indice -2, l’avant-avant dernier l’indice -3, etc.


```python
>>> jedi[0]
```


```python
>>> jedi[-9]
```


```python
>>> jedi[-1]
```


```python
>>> tailleJedi[-1]
```

## ![python_titre_bleu](images/Python_titre_bleu.png) **Les méthodes de l'objet <class 'list'>**


```python
dir(list) # Les méthodes de l'objet liste.
```

### ![python_dossier](images/python_dossier.png) **Longueur d'une liste**


```python
jedi = ['Plo Koon', 'Qui-Gon Jinn', 'Obi-Wan Kenobi', 'Ki-Adi-Mundi', 'Anakin Skywalker', 'Luke Skywalker', 'Ahsoka Tano', 'Mace Windu', 'Yoda']
print(len(jedi))
```

### ![python_dossier](images/python_dossier.png) **Ajouter en fin de liste**


```python
liste = [1, 3, 5]
liste.append(2)
print(liste)
```

### ![python_dossier](images/python_dossier.png) **Vider une liste**


```python
liste.clear()
print(liste)
```

### ![python_dossier](images/python_dossier.png) **La méthode ``insert()`` insère un objet dans une liste avec un indice déterminé**


```python
liste = [1, 3, 5]
liste.insert(1, 9)
print(liste)
```

### ![python_dossier](images/python_dossier.png) **La méthode ``remove()`` supprime un élément d’une liste à partir de sa valeur.**


```python
liste = [1, 3, 5]
liste.remove(3)
print(liste)
```

### ![python_dossier](images/python_dossier.png) **La méthode sort() trie une liste**


```python
liste = [2, 3, 4, 1]
liste.sort(reverse=False)
print(liste)
liste.sort(reverse=True)
print(liste)
```

### ![python_dossier](images/python_dossier.png) **La méthode reverse() inverse une liste**


```python
liste = [2, 3, 4, 1]
liste.reverse()
print(liste)
```

### ![python_dossier](images/python_dossier.png) **La méthode count() compte le nombre d’éléments (passés en argument) dans une liste**


```python
liste = [2, 3, 4, 1, 2, 7, 2]
liste.count(2)
```

> De nombreuses méthodes ci-dessus (.append(), .sort(), etc.) modifient la liste mais ne renvoient rien, c’est-à-dire
qu’elles ne renvoient pas d’objet récupérable dans une variable. 
>
> Il s’agit d’un exemple d’utilisation de méthode (donc de fonction particulière) qui fait une action mais qui ne renvoie rien. 
>
> Pensez-y dans vos utilisations futures des listes.
>
> Certaines méthodes ou instructions des listes décalent les indices d’une liste (par exemple .insert(), del, etc.).

### ![python_dossier](images/python_dossier.png) **Premier indice d'un élément d'une liste**


```python
tailleJedi.index(1.88)
```

### ![python_dossier](images/python_dossier.png) **Attention! Les dangers de l'affectation avec les listes**


```python
%%tutor --lang python3
liste = [1, 3, 5]
liste2 = liste
liste2[1] = 3

```

### ![python_dossier](images/python_dossier.png) **Copier une liste : Shallow copy**


```python
%load_ext tutormagic
```


```python
%%tutor --lang python3
liste = [1, 3, 5]
liste1 = liste
liste2 = liste.copy()
liste3 = liste[:]
```

## ![python_titre_bleu](images/Python_titre_bleu.png) **Exercice 1**

Écrire la fonction ``` positionsElementListe(element, liste)```

La fonction ``positionsElementListe(liste)`` permet de déterminer les positions d'un même élément dans une liste.
- **@param** : ``element`` objet recherché dans la liste.
- **@param** : ``liste`` est de type ``<class 'list'>``.
- **@return** : Une liste  ``<class 'list'>`` des indices de l'élément recherché.

## ![python_titre_bleu](images/Python_titre_bleu.png) **Exercice 2** 

Écrivez la fonction ``infosListe`` qui demande à l'utilisateur d'entrer une liste d'entiers et qui affiche :

1. Le nombre total d’éléments de la liste.
2. Le dernier élément de la liste.
3. La liste dans l'ordre inverse.
4. Oui si la liste contient un 5 et non sinon.
5. Le nombre de cinq dans la liste.
6. Supprimez les premier et dernier éléments de la liste, triez les éléments restants et imprimez le résultat.

## ![python_titre_bleu](images/Python_titre_bleu.png) **Exercice 3** 

Trouvez le nombre mystère qui répond aux conditions suivantes :
 + Il est composé de 3 chiffres.
 + Il est strictement inférieur à 300.
 + Il est pair.
 + Deux de ses chiffres sont identiques.
 + La somme de ses chiffres est égale à 7.

## ![python_titre_bleu](images/Python_titre_bleu.png) **Exercice 4** 
Soit la liste de nombres $`[5, 1, 1, 2, 5, 6, 3, 4, 4, 4, 2]`$
Retirez les doublons de cette liste, triez-la et affichez-la.
