# ![python ](images/Python_technologie.png)**Les fonctions en programmation Python**

En programmation, les fonctions sont très utiles pour réaliser plusieurs fois la même opération au sein d’un programme.
Elles rendent également le code plus lisible et plus clair en le fractionnant en blocs logiques.

## ![python_titre_bleu](images/Python_titre_bleu.png)  **Les fonctions en vidéo**

[![fonction](images/fonction.png)](https://www.dropbox.com/s/zijmpa25ffqn3pt/fonctionsgit.mp4?dl=0)

## ![python_titre_bleu](images/Python_titre_bleu.png)  **La fonction built-in ``len``**

![apprendre](images/apprendre.png) **Observe attentivement**


```python
help(len)
```


```python
len('DarkSATHI')
```


```python
len('DarkSATHI Li')
```

**Une fonction est à vos yeux une sorte de boîte noire:**

- Une fonction effectue une tâche. Pour cela, elle reçoit éventuellement des arguments et renvoie éventuellement quelque chose. L’algorithme utilisé au sein de la fonction n’intéresse pas directement l’utilisateur. Par exemple, il est inutile de savoir comment la fonction ``len()`` calcule le nombre d'items d'un objet. On a juste besoin de savoir qu’il faut lui passer en argument ``'DarkSATHI'`` et qu’elle renvoie le nombre de caractères de cette chaîne.
- Chaque fonction effectue en général une tâche unique et précise. Si cela se complique, il est plus judicieux d’écrire plusieurs fonctions (qui peuvent éventuellement s’appeler les unes les autres). Cette modularité améliore la qualité générale et la lisibilité du code. Vous verrez qu’en Python, les fonctions présentent une grande flexibilité.

![apprendre](images/apprendre.png) **Observe attentivement**

Python a un certain nombre de fonctions qui sont toujours disponibles. Ces fonctions sont appelées fonctions intégrées(built-in). Par exemple, la fonction ``print()`` imprime l'objet donné sur la sortie standard (écran) ou sur le flux de texte d'un fichier.


```python
import builtins
```


```python
dir(builtins)
```

## ![python_titre_bleu](images/Python_titre_bleu.png) **Définir une fonction**

Pour définir une fonction, Python utilise le mot-clé def et si on veut que celle-ci renvoie quelque chose, il faut utiliser le
mot-clé **``return``**. 


```python
from math import sqrt # Appel de la fonction sqrt du module math : racine carrée

def longueur_vecteur(coordonnees):
    
    '''
    La fonction longueur_vecteur calcule la longueur d'un vecteur dans le plan.
    
    @Paramètre : coordonnees est une variable qui référence un objet tuple dont les items sont
    les coordonnées du vecteur.
    @Retourne : La fonction retourne un objet de type <float> qui est la longueur du vecteur
        
    @Exemples : 
        >>> vecteur = longueur_vecteur((0, 0))
        >>> vecteur
        0.0
        >>> vecteur = longueur_vecteur((3, 4))
        >>> vecteur
        5.0
        
    '''
    x, y = coordonnees
    longueur = sqrt(x**2 + y**2)     
        
    return longueur  
```


```python
if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose = True, optionflags=doctest.NORMALIZE_WHITESPACE)
```


```python
longueur_vecteur((2, 4))
```

### ![python_dossier](images/python_dossier.png) **Définition**

<p style="color:red">En programmation informatique, la factorisation de code consiste à rassembler les suites d'instructions identiques dispersées dans un programme en une fonction, ou procédure pour améliorer la lisibilité du code et en faciliter la correction et les modifications ultérieures.<\p>


> Lorsqu’on définit une fonction, ``def fct(x, y)``, les arguments x et y sont appelés arguments positionnels (en anglais
positional arguments). Il est strictement obligatoire de les préciser lors de l’appel de la fonction. De plus, il est nécessaire de
respecter le même ordre lors de l’appel que dans la définition de la fonction. 

> Un argument défini avec une syntaxe, ``def fct(arg=val)``, est appelé argument par mot-clé (en anglais keyword argument). Le passage d’un tel argument lors de l’appel de la fonction est facultatif. Ce type d’argument ne doit pas être confondu avec les arguments positionnels présentés ci-dessus, dont la syntaxe est ``def fct(arg)``

> Les arguments positionnels doivent toujours être placés avant les arguments par mot-clé.

## ![python_titre_bleu](images/Python_titre_bleu.png) **Attentions! Variables locales VS variables globales**

Lorsque Python rencontre une variable, il va traiter la résolution de son nom avec des priorités particulières. D’abord il
va regarder si la variable est locale, puis si elle n’existe pas localement, il vérifiera si elle est globale et enfin si elle n’est pas
globale, il testera si elle est interne (par exemple la fonction ``len`` est considérée comme une fonction interne à Python, elle
existe à chaque fois que vous lancez Python). On appelle cette règle la règle **LGI** pour locale, globale, interne.

### ![python_dossier](images/python_dossier.png) **Une vidéo pour mieux comprendre**

[![LEG](images/LEG.png)](https://www.dropbox.com/s/wl3x1ky1y6r065i/LEGGit.mp4?dl=0)


```python
%load_ext tutormagic
```


```python
%%tutor --lang python3

a = 'scope global'

def fois_trois(x):
    x = x * 3
    print(f"Dans le scope de la fonction fois_trois, x vaut {x} et son id vaut {id(x)}")
    a = 'scope local de la fonction fois_trois'
    return(x, a, id(a))

x = 2
print(f"Dans le scope global, x vaut {x} et son id vaut {id(x)}")
print(f"Dans le scope global, a vaut {a} et son id vaut {id(a)}")
resultat = fois_trois(6)
```

------------------
# A vous de jouer !

## ![python_titre_bleu](images/Python_titre_bleu.png) **Exercices**

1. **Écrivez une fonction appelée ``sum_digits`` qui a unparamètre un nombre entier ``num`` et renvoie la somme des chiffres de ``num``.**
2. **Écrivez une fonction appelée ``rectangle`` qui prend deux entiers ``longueur`` et ``largeur`` en arguments et en affiche une boîte contenant des astérisques.**
3. **Que va afficher ce code ?**


```python
def localVariable():
    x = 2
    print('x vaut {} dans la fonction '. format (x))

localVariable()
print(x)
```

> Il est très important lorsque l’on manipule des fonctions de connaître la portée des variables (scope en anglais), c’est-à-dire
savoir là où elles sont visibles. 
>
> Les variables créées au sein d’une fonction ne sont pas visibles à l’extérieur de
celle-ci car elles sont locales à la fonction.

4. **Que va afficher ce code ?**


```python
def argument(x):
    print('x vaut {} dans la fonction '. format (x))

argument(x)
print(x)
```

>Lorsque Python exécute le code de la fonction, il connaît le contenu de la variable ``x``. Par contre, de retour dans le module
principal (dans ce cas, il s’agit de l’interpréteur Python), il ne la connaît plus, d’où le message d’erreur.
>
>De même, une variable passée en argument est considérée comme locale lorsqu’on arrive dans la fonction.

5. **Que va afficher ce code ?**


```python
def globaleVariable():
    print(x)

x = 3
globaleVariable()
print(x)
```

> Lorsqu’une variable est déclarée dans le programme principal, elle est visible dans celui-ci ainsi que dans toutes les
fonctions. On parle de variable globale.

6. **Que va afficher ce code ?**


```python
def globaleProtégee():
    x = x + 1

x = 1
ma_fonction()
```

> Python ne permet pas la modification d’une variable globale dans une fonction, on utilisera le code suivant pour produire l'effet recherché.


```python
def globaleProtégee():
    return x + 1

x = 1
x = ma_fonction()
```

## ![attention](images/attention.png) Attention au types modifiables que sont, par exemple, les listes!


```python
%load_ext tutormagic
```


```python
%%tutor --lang python3

def listeModifiable():
    liste[2] = 5

liste = [1, 1, 1, 1, 1]
listeModifiable()
```


```python
%%tutor --lang python3

def listeModifiable(liste):
    liste[2] = 5

liste = [1, 1, 1, 1, 1]
listeModifiable()
```

**On utilisera une Shallow copy**


```python
%%tutor --lang python3

def listeProtegee(liste):
    liste[2] = 5
    return liste

liste = [1, 1, 1, 1, 1]
liste2 = listeProtegee(liste[:])
```



<iframe
            width="100%"
            height="350"
            src="http://pythontutor.com/iframe-embed.html#code=%0Adef%20listeProtegee%28liste%29%3A%0A%20%20%20%20liste%5B2%5D%20%3D%205%0A%20%20%20%20return%20liste%0A%0Aliste%20%3D%20%5B1%2C%201%2C%201%2C%201%2C%201%5D%0Aliste2%20%3D%20listeProtegee%28liste%5B%3A%5D%29%0A&origin=opt-frontend.js&cumulative=false&heapPrimitives=false&textReferences=false&curInstr=0&&verticalStack=false&py=3&rawInputLstJSON=%5B%5D&codeDivWidth=50%25&codeDivHeight=100%25"
            frameborder="0"
            allowfullscreen
></iframe>
        

